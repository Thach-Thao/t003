<?php $id="index";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="p-index">
	<div class="p-note ">
		<div class="p-note__title">CAMPAIGN</div>
		<div class="p-note__cont">
			<p>キャンペーン</p>
		</div>
	</div>

	<div class="p-index01 ">
		<div class="l-container">
			<div class="p-index01__cont">
				<div class="only-pc">
					<div class="c-box c-box--item1">
						<div class="c-box__num">
							<p><img src="/assets/image/index/No1.png" width="85" height="85"></p>
						</div>
						<div class="c-box__txt01">
							事前お申込キャンペーン
						</div>
						<div class="c-box__txt02">
							<img src="/assets/image/index/img04.png" width="480" height="60">
						</div>
						<div class="c-box__txt03">
							<p class="c-box__txt03--01">入会金・月会費</p>
							<p class="c-box__txt03--02">4～５月分無料！</p>
						</div>
					</div>

					<div class="c-box c-box--item2 ">
						<div class="c-box__num">
							<p><img src="/assets/image/index/No2.png" width="85" height="85"></p>
						</div>
						<div class="c-box__txt01">
							グランドオープン<br>入会キャンペーン
						</div>
						<div class="c-box__txt02">
							<img src="/assets/image/index/img05.png" width="480" height="60">
						</div>
						<div class="c-box__txt03">
							<p>入会金<span class="u-bigtxt">無料!</span></p>
							<p>4～５月分
							<span class="u-bigtxt">3,000円<span class=u-spink>(税抜)</span>!!</span></p>
						</div>
					</div>

					<div class="c-box c-box--item3">
						<div class="c-box__num">
							<p><img src="/assets/image/index/No3.png" width="85" height="85"></p>
						</div>
						<div class="c-box__txt01">
							グランドオープン<br>体験レッスンキャンペーン
						</div>
						<div class="c-box__txt02">
							<img src="/assets/image/index/img05.png" width="480" height="60">
						</div>
						<div class="c-box__txt03">
							<p>体験レッスン料<span class="u-bigtxt">無料!</span></p>
						</div>
					</div>
				</div>

				<div class="only-sp">
					<div class="c-box01">
						<img src="/assets/image/index/box1.png" width="604" height="401">
					</div>
					<div class="c-box02">
						<img src="/assets/image/index/box2.png" width="299" height="436">
					</div>
					<div class="c-box03">
						<img src="/assets/image/index/box3.png" width="299" height="436">
					</div>
				</div>

			</div>
			<div class="p-index01__btn">
				<a href="">見学・体験レッスンのご予約はこちら</a>
			</div>
		</div>
	</div>

	<div class="p-index02">
		<div class="l-container">
			<div class="c-title01">
				<img src="/assets/image/index/title01.png" width="379" height="68" alt="">
			</div>

			<div class="p-index02__cont">
				<div class="p-index02__ttl">
					事前申し込み＜期間限定＞
				</div>
				<div class="p-index02__cont--img">
					<div class="img-item u-lf">
						<p class="only-pc"><img src="/assets/image/index/img01.png" width="492" height="62" alt=""></p>
						<p class="only-sp"><img src="/assets/image/index/img01_sp.png" width="275" height="229" alt=""></p>
					</div>
					<div class="img-item">
						<p class="only-pc"><img src="/assets/image/index/img02.png" width="495" height="62" alt=""></p>
						<p class="only-sp"><img src="/assets/image/index/img02_sp.png" width="275" height="229" alt=""></p>
					</div>
				</div>
			</div>

			<div class="p-index02__cont01">
				<div class="p-index02__ttl">
					通常料金 ※(税抜)
				</div>

				<div  class="p-index02__tbl only-pc">
					<table>
						<tr>
							<th></th>
						    <th>会員</th>
						    <th>ビジター</th> 
						    <th>フリー練習費</th>
						 </tr>
						<tr>
						    <td>⼊会⾦</td>
						    <td>￥5,000／1回</td>
						    <td>−</td>
						    <td>−</td>
						</tr>
						<tr>
						    <td>
						    	<p class="col1">レギュラー</p>
						    	<p class="col2">全⽇利⽤可</p>
						    </td>
						    <td>￥10,000（税抜）／⽉額</td>
						    <td>−</td>
						    <td class="u-red">無料</td>
						</tr>
						<tr>
						    <td>
						    	<p class="col1">デイタイム</p>
						    	<p class="col2">平⽇昼限定（11:00〜17:00）</p>
						    </td>
						    <td>￥7,000（税抜）／⽉額</td>
						    <td>−</td>
						    <td>時間外有料</td>
						</tr>
						<tr>
						    <td>
						    	<p class="col1">ジュニア</p>
						    	<p class="col2">⽊曜⽇限定 17：00〜18：00<br>中学3年生まで</p>
						    </td>
						    <td>￥6,000（税抜）／⽉額</td>
						    <td>−</td>
						    <td>時間外有料</td>
						</tr>
						<tr>
						    <td>
						    	<p class="col1">ホリデイ</p>
						    	<p class="col2">⼟・⽇・祝のみ終⽇利⽤可</p>
						    </td>
						    <td>￥8,000（税抜）／⽉額</td>
						    <td>−</td>
						    <td>時間外有料</td>
						</tr>
						<tr>
						    <td>
						    	<p class="col1">レッスン4</p>
						    	<p class="col2">レッスン4回<br>
						    	（スタンプカード制／有効期間：3ヶ月）</p>
						    </td>
						    <td>−</td>
						    <td>￥8,000（税抜）／1セット</td>
						    <td>有料</td>
						</tr>
						<tr>
						    <td>
						    	<p class="col1">フリー練習</p>
						    	<p class="col2">空打席があれば利⽤可<br>
						    	※レッスンは含まず</p>
						    </td>
						    <td>
						    	<p class="u-strikethrough">￥1,000（税抜）／1回</p><br>
						    	<p class="u-red">プレオープン期間限定 ￥0</p>
						    </td>
						     <td>
						    	<p class="u-strikethrough">￥1,500（税抜）／1回</p><br>
						    	<p class="u-red">プレオープン期間限定 ￥0</p>
						    </td>
						    <td>有料</td>
						</tr>
						<tr>
						    <td>
						    	<p class="col1">体験レッスン</p>
						    	<p class="col2">全⽇利⽤可</p>
						    </td>
						    <td>−</td>
						     <td>￥2,000（税抜）／1回</td>
						    <td>−</td>
						</tr>
					</table>
				</div>

				<div class="p02-accodition only-sp">
					<div class="p02-accodition__item">
						<button class="p02-accodition__button">
							<p>入会金</p>
						</button>
						<div  class="p02-accodition__panel">
							<div class="ac-row">
								<p class="ac-row__left">⼊会⾦</p>
								<p class="ac-row__right">￥5,000（税抜）／1回</p>
							</div>
						</div>
					</div>

					<div class="p02-accodition__item">
						<button class="p02-accodition__button">
							<p>レギュラー<span>全⽇利⽤可</span></p>
						</button>
						<div  class="p02-accodition__panel">
							<div class="ac-row">
								<p class="ac-row__left">会員</p>
								<p class="ac-row__right">￥10,000（税抜）／⽉額</p>
							</div>
							<div class="ac-row">
								<p class="ac-row__left">ビジター</p>
								<p class="ac-row__right">ー</p>
							</div>
							<div class="ac-row">
								<p class="ac-row__left">フリー練習費</p>
								<p class="ac-row__right">無料</p>
							</div>
							<div class="ac-row">
								<p class="ac-row__left">打席料・ボール代</p>
								<p class="ac-row__right">無料</p>
							</div>
						</div>
					</div>

					<div class="p02-accodition__item">
						<button class="p02-accodition__button">
							<p>デイタイム<span>平⽇昼限定（11:00〜17:00）</span></p>
						</button>
						<div  class="p02-accodition__panel">
							<div class="ac-row">
								<p class="ac-row__left">会員</p>
								<p class="ac-row__right">￥7,000（税抜）／⽉額</p>
							</div>
							<div class="ac-row">
								<p class="ac-row__left">ビジター</p>
								<p class="ac-row__right">ー</p>
							</div>
							<div class="ac-row">
								<p class="ac-row__left">フリー練習費</p>
								<p class="ac-row__right">時間外有料</p>
							</div>
						</div>
					</div>

					<div class="p02-accodition__item">
						<button class="p02-accodition__button">
							<p>ジュニア<span>⽊曜⽇限定 17：00〜18：00<br>中学3年生まで</span></p>
						</button>
						<div  class="p02-accodition__panel">
							<div class="ac-row">
								<p class="ac-row__left">会員</p>
								<p class="ac-row__right">￥6,000（税抜）／⽉額</p>
							</div>
							<div class="ac-row">
								<p class="ac-row__left">ビジター</p>
								<p class="ac-row__right">ー</p>
							</div>
							<div class="ac-row">
								<p class="ac-row__left">フリー練習費</p>
								<p class="ac-row__right">時間外有料</p>
							</div>
						</div>
					</div>

					<div class="p02-accodition__item">
						<button class="p02-accodition__button">
							<p>ホリデイ<span>⼟・⽇・祝のみ終⽇利⽤可</span></p>
						</button>
						<div  class="p02-accodition__panel">
							<div class="ac-row">
								<p class="ac-row__left">会員</p>
								<p class="ac-row__right">￥8,000（税抜）／⽉額</p>
							</div>
							<div class="ac-row">
								<p class="ac-row__left">ビジター</p>
								<p class="ac-row__right">ー</p>
							</div>
							<div class="ac-row">
								<p class="ac-row__left">フリー練習費</p>
								<p class="ac-row__right">時間外有料</p>
							</div>
						</div>
					</div>

					<div class="p02-accodition__item">
						<button class="p02-accodition__button">
							<p>レッスン4<span>レッスン4回<br>（スタンプカード制／有効期間：3ヶ月）</span></p>
						</button>
						<div  class="p02-accodition__panel">
							<div class="ac-row">
								<p class="ac-row__left">会員</p>
								<p class="ac-row__right">ー</p>
							</div>
							<div class="ac-row">
								<p class="ac-row__left">ビジター</p>
								<p class="ac-row__right">￥8,000（税抜）／1セット</p>
							</div>
							<div class="ac-row">
								<p class="ac-row__left">フリー練習費</p>
								<p class="ac-row__right">有料</p>
							</div>
						</div>
					</div>

					<div class="p02-accodition__item">
						<button class="p02-accodition__button">
							<p>フリー練習<span>空打席があれば利⽤可<br>※レッスンは含まず</span></p>
						</button>
						<div  class="p02-accodition__panel">
							<div class="ac-row">
								<p class="ac-row__left">会員</p>
								<p class="ac-row__right">
									<span class="u-strikethrough">￥1,000（税抜）／1回</span><br>
									<span class="u-red">プレオープン期間限定 ￥0</span>
								</p>
							</div>
							<div class="ac-row">
								<p class="ac-row__left">ビジター</p>
								<p class="ac-row__right">
									<span class="u-strikethrough">￥1,500（税抜）／1回</span><br>
									<span class="u-red">プレオープン期間限定 ￥0</span>
								</p>
							</div>
							<div class="ac-row">
								<p class="ac-row__left">フリー練習費</p>
								<p class="ac-row__right">有料</p>
							</div>
						</div>
					</div>

					<div class="p02-accodition__item">
						<button class="p02-accodition__button">
							<p>体験レッスン<span>全⽇利⽤可</span></p>
						</button>
						<div  class="p02-accodition__panel">
							<div class="ac-row">
								<p class="ac-row__left">会員</p>
								<p class="ac-row__right">ー</p>
							</div>
							<div class="ac-row">
								<p class="ac-row__left">ビジター</p>
								<p class="ac-row__right">
									￥2,000（税抜）／1回
								</p>
							</div>
							<div class="ac-row">
								<p class="ac-row__left">フリー練習費</p>
								<p class="ac-row__right">ー</p>
							</div>
						</div>
					</div>

				</div>

				<div class="p-index02__txt">
					<p class="p-index02__txt--greem">レッスン・フリー利用可能時間</p>
					<p class="text">1時間（準備時間含む、会員・ビジター問わず）</p>
					<p class="p-index02__txt--blue">学生割引</p>
					<p class="text">上記料金より50％OFF（会員カテゴリのみ適用※フリー練習含まず）</p>
				</div>
			</div>
		</div>
	</div>

	<div class="p-index03">
		<div class="l-container">
			<div class="p-index03__txt01">
				見学・体験のご予約もこちらから!
			</div>
			<div class="p-index03__txt02">
				Reservation<br class="only-sp"><span>レッスンWeb予約</span>
			</div>
			<div class="p-index03__txt03">
				当スクールは予約制となっております。<br>お電話、または予約システムよりご予約をお願い致します。
			</div>
			<div class="p-index03__grbtn">
				<div class="col1">
					<a href="" class="p-index03__btn01">ご予約はこちら</a>
				</div>
				<div class="col2">
					<a href="" class="p-index03__btn02">
						<span class="btnText">ご予約はこちら</span><br>
						<span class="btnNum">0493ｰ23ｰ8015</span>
					</a>
					<div class="p-index03__txt04">
						平日11:00～21:00（昼休憩13:00～14:00）／<br>土・日・祝10:00～19:00<br>定休日 火曜日
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="p-index04">
		<div class="l-container">
			<div class="c-title01">
				<img src="/assets/image/index/title02.png" width="735" height="68" alt="">
			</div>
			<div class="p-index04__block">
				<div class="colLeft">
					<img src="/assets/image/index/img03.png" width="272" height="204" alt="">
				</div>
				<div class="colRight">
					<div class="colRight__txt01">
						<p class="only-pc">矢島　嘉彦</p>
						<p class="only-sp">講師名が入ります</p>
					</div>
					<p class="colRight__txt02">1,000人以上のレッスン経験があり、初心者から上級者までそれぞれの悩みや疑問を解決し指導します。</p>
					<ul class="colRight__list">
						<li><span>
							公益社団法人日本プロゴルフ協会
						</span></li>
						<li><span>
							高校生からゴルフを始め、2006年にティーチングプロとして社団法人日本プロゴル<br>フ協会の会員となる
						</span></li>
					</ul>
				</div>
			</div>
			<div class="c-title01">
				<img src="/assets/image/index/title03.png" width="609" height="68" alt="">
			</div>

			<div class="p-index04__slide only-pc">
				<div class="slider-nav">
				    <div><img src="/assets/image/index/slide01.png" ></div>
				    <div><img src="/assets/image/index/slide02.png"></div>
				    <div><img src="/assets/image/index/slide03.png"></div>
				    <div><img src="/assets/image/index/slide04.png"></div>
				    <div><img src="/assets/image/index/slide05.png"></div>
				    <div><img src="/assets/image/index/slide01.png" ></div>
				    <div><img src="/assets/image/index/slide01.png"></div>
			  	</div>
			</div>

			<div class="p-index04__cont only-sp">
				 <div class="p04-imgList">
				 	<img src="/assets/image/index/slide01.png" width="272" height="204" >
				 </div>
			    <div class="p04-imgList">
			    	<img src="/assets/image/index/slide02.png" width="272" height="204">
			    </div class="p04-imgList">
			    <div class="p04-imgList">
			    	<img src="/assets/image/index/slide03.png" width="272" height="204">
			    </div>
			    <div class="p04-imgList">
			    	<img src="/assets/image/index/slide04.png" width="272" height="204">
			    </div>
			    <div class="p04-imgList">
			    	<img src="/assets/image/index/slide05.png" width="272" height="204">
			    </div>
			</div>
		</div>
	</div>

	<div class="p-index05">
		<div class="p-index05__map">
			<div class="p-index05__title">
				アクセスマップ
			</div>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3226.3872958217494!2d139.40051331574838!3d36.035252730113235!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe843e88a64e460b5!2z44Kk44Oz44OJ44Ki44K044Or44OV44K544Kv44O844OrQVJST1dT5p2x5p2-5bGx5bqX!5e0!3m2!1sen!2s!4v1528450737124" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
		<div class="p-index05__infor">
			<div class="infor-item">
				<div class="infor-item__left">
					<img src="/assets/image/index/icon03.png" width="17" height="16">営業時間
				</div>
				<div class="infor-item__right">
					平日11:00〜21:00(昼休憩13:00〜14:00)<br>土・日・祝10:00〜19:00
				</div>
			</div>
			<div class="infor-item">
				<div class="infor-item__left">
					<img src="/assets/image/index/icon04.png" width="16" height="15">定休日
				</div>
				<div class="infor-item__right">火曜日</div>
			</div>
			<div class="infor-item">
				<div class="infor-item__left">
					<img src="/assets/image/index/icon05.png" width="16" height="16">レッスン・フリー<br>利用可能時間
				</div>
				<div class="infor-item__right">会員・ビジター問わず1時間(準備時間含む)</div>
			</div>
			<div class="infor-item">
				<div class="infor-item__left">
					<img src="/assets/image/index/icon06.png" width="16" height="14">TEL
				</div>
				<div class="infor-item__right">0493-23-8015</div>
			</div>
			<div class="infor-item">
				<div class="infor-item__left">
					<img src="/assets/image/index/icon07.png" width="17" height="17">住所
				</div>
				<div class="infor-item__right">
					〒355-0028<br>埼玉県東松山市箭弓町1-13-16 安福ビル1F
				</div>
			</div>
			<div class="p-index05__bg only-pc">
				<img src="/assets/image/index/bg06.png" width="760" height="417">
			</div>
		</div>
	</div>

	<div class="p-index06">
		<div class="l-container">
			<div class="p-index06__title01">
				Information
			</div>
			<div class="p-index06__title02">お知らせ</div>
			<div class="p-index06__cont">
				<div class="infor-item">
					<p class="infor-item__date">
						 2018年0月0日
					</p>
					<p class="infor-item__ttl">
						<a href="">
							インドアゴルフスクール ARROWS 東松山店 サイト公開しました。
						</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>