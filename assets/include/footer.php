<footer>
	<div class="c-footer">
		<div class="c-footer01">
			<div class="c-footer01__img">
				<img src="/assets/image/common/f_img01.png" width="480" height="90" alt="">
			</div>
			<div class="c-footer01__infor">
				〒355-0028 埼玉県東松山市箭弓町1-13-16 安福ビル1F　TEL.0493-23-8015<br>
				営業時間：［平日］11:00〜21:00（昼休憩13:00〜14:00）／［土・日・祝］10:00〜19:00　火曜定休<br>
				レッスン・フリー利用可能時間：会員・ビジター問わず1時間（準備時間含む）
			</div>
		</div>
		<div class="c-footer02">
			<p>Copyright (c) Shin Corporation All Rights Reserved.</p>
		</div>
		<div href="#" class="page-top" id="back-to-top" title="Back to top"></div>
	</div>
</footer>

<script type="text/javascript">
	$('.slider-nav').slick({
	   	slidesToShow: 5,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 2000,
		arrows: false
	});

	//croll to top
	$(document).ready(function(){
		$("#back-to-top").hide();

		$(function () {
			$(window).scroll(function () {
				if ($(this).scrollTop() > 30) {
					$('#back-to-top').show();
				} else {
					$('#back-to-top').hide();
				}
			});

			$('#back-to-top').click(function () {
				$('body,html').animate({
					scrollTop: 0
				}, 800);
				return false;
			});
		});
	});

	// acodition
	var acc = document.getElementsByClassName("p02-accodition__button");
	var i;

	for (i = 0; i < acc.length; i++) {
	 	acc[i].onclick = function() {
	    	var active = document.querySelector(".p02-accodition-active");
	    	if (active && active != this) {
	      		active.classList.remove("p02-accodition-active");
	      		active.nextElementSibling.classList.remove("p02-acodition-show");
	    	}
	   		this.classList.toggle("p02-accodition-active");
	    	this.nextElementSibling.classList.toggle("p02-acodition-show");
	  	}
	}
</script>

</body>
</html>