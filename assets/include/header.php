<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/meta.php'); ?>

<link href="/assets/css/style.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<link href="https://fonts.googleapis.com/css?family=Roboto:300i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400i" rel="stylesheet">

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
</head>

<body class="page-<?php echo $id; ?>">

<header>
	<div class="c-headerPC">
		<div class="c-headerPC__MV">
			<div class="c-headerPC__banner">
				<p class="c-headerPC__banner--01 only-pc"><img src="/assets/image/common/h_img01.png" width="930" height="344" alt=""></p>
				<p class="c-headerPC__banner--02 only-sp">
					<img src="/assets/image/common/h_img_SP01.png" width="640" height="201" alt="">
				</p>
				<p class="c-headerPC__banner--03 only-sp">
					<img src="/assets/image/common/h_img_SP02.png" width="640" height="201" alt="">
				</p>
			</div>
		</div>
		<nav class="c-nav only-pc">
			<ul>
				<li><a href="">キャンペーン情報</a></li>
				<li><a href="">ご利用料金</a></li>
				<li><a href="">ご予約</a></li>
				<li><a href="">講師紹介</a></li>
				<li><a href="">店舗のご案内</a></li>
			</ul>
		</nav>
	</div>
</header>